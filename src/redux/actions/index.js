import axios from 'axios';
export const ACTION_GET_RESTAURANTS = 'ACTION_GET_RESTAURANTS';


export const getRestaurants = () => {
    axios.get("http://194.67.90.107/api/places/")
    .then(response => {
        const restaurants = response.data;
        return {
            type: ACTION_GET_RESTAURANTS,
            payload: restaurants
        }
    });
};