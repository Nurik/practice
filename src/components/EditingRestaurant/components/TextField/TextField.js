import React from 'react';

class TextField extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <h3>{this.props.label}</h3>
                <input type="text" value=""/>
           </>
        );
    }
}

export default TextField;