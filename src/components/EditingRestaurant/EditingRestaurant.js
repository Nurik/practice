import React from 'react';
import TextField from './components/TextField/TextField'

class EditingRestaurant extends React.Component{
    render() {
        return (
            <div>
                Страница редактирования заведения
                <TextField
                label="Название"
                />
                <TextField
                label="Адрес"
                />
            </div>
        )
    };
}

export default EditingRestaurant;