import React from 'react';
import RestaurantCard from '../RestaurantCard/RestarauntCard';
import { Switch, Route, Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getRestaurants} from '../../redux/actions/index';

class RestaurantsContainer extends React.Component {
    constructor(props) {
        super(props);
        getRestaurants();
    }
    render() {
        getRestaurants();
        const {restaurants} = this.props;
        return (
            <div>
                <div className="title-restaurants">
                    <h3>Мои заведения</h3>
                </div>
                    <div className='container-restaurants'>
                    {
                        restaurants.map((restaurant, index) => {
                            console.log(restaurant);
                            return this.renderRestaurant(index);
                        })
                    }
                    <Link className="link restaurant" to='/editing'>
                        <div className="plus-box">
                            <span className="plus">+</span>
                        </div>
                    </Link>
                </div>
            </div>

        );
    }

    renderRestaurant(index) {
        return <RestaurantCard configuration={this.props.restaurants[index]}/>
    }
}


const putStateToProps = (state) => {
   return {
        restaurants: state.restaurants
   }
};

const putActionsToProps = (dispatch) => {
    return {
        getRestaurants: bindActionCreators(getRestaurants, dispatch)
    }
}

export default connect(putStateToProps, putActionsToProps)(RestaurantsContainer);