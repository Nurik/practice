import React from 'react';

class Header extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="header">
                    <span className="logo">Canyon</span>
                    <div className="user">
                        <span>Вы вошли как: Иван{} | <a className="logout">Выйти</a></span>
                    </div>
                </div>

            </div>
        );
    }
}

export default Header;