import React from 'react'
import './App.scss'
import { Switch, Route, Link} from 'react-router-dom'
import Header from '../Header/Header';
import RestaurantsContainer from '../RestaurantsContainer/RestarauntsContainer';
import EditingRestaurant from "../EditingRestaurant/EditingRestaurant";

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            restaurants: [
                {
                    name: "Съем жирафа",
                    location: "г.Красноярск, проспект Свободный, 27г",
                },
                {
                    name: "Yellow snowball",
                    location: "г.Красноярск, улица Ленина, 47",
                },
                {
                    name: "Yellow snowball",
                    location: "г.Красноярск, улица Ленина, 47",
                }
            ]
        }
    }
    render() {
        return (
            <div className="App">
                <Header/>
                <Switch>
                    <Route  path='/restaurants' render={ (props) => (<RestaurantsContainer restaurants={this.state.restaurants}/>)}/>
                    <Route path='/editing' render={(props) => <EditingRestaurant/> }/>
                </Switch>
            </div>
        );
    }
}

export default App;
