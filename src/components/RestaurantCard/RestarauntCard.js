import React from 'react';
import { Switch, Route, Link} from 'react-router-dom'


class RestaurantCard extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Link className="link restaurant" to='/editing'>
                <div>
                    <div className="restaurant__header">
                        <div className="restaurant__title">{this.props.configuration.name}</div>
                        <div className="restaurant__image"><img src={this.props.configuration.image} alt=""/></div>
                    </div>
                    <div className="restaurant__location"><span className="address">Адрес: </span>{this.props.configuration.address}</div>
                    <div className="restaurant__time"><span>Часы работы: </span>{this.props.configuration.from_hour} - {this.props.configuration.to_hour}</div>

                </div>
            </Link>
        );
    }
}

export default RestaurantCard;